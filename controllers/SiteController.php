<?php

namespace app\controllers;

use app\models\Empleado;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    //Accion creada para probar el active query y consultas.
    public function actionEmpleados() {
        // Active query, clase de yii para consultas sql.
        $aq = Empleado::find(); //select * from empleado;
        $vectorAR = $aq->all(); //Consulta ejecutada.

        /* $vectorAR[1]->nombre; //Leer.
          $vectorAR[1]->nombre = 'Jose'; //Escribir. */

        $vectorArrays = $aq->asArray()->all();

        //Leer.
        //echo$vectorArrays[1]['nombre']
        //Empleado::findAll([1]);

        /* Empleado::find()
          ->where(['codigo' => 1])
          ->all(); */

        /* var_dump(Empleado::find()
          ->where("codigo=1")
          ->all()); */

        //Quiero mostrar los empleados cuyos codigos estan entre 1 y 5.
        $consulta1 = Empleado::find()
                ->where(['between', 'codigo', 1, 5]); //Utilizando el parametro between buscamos todos los codigos entre el 1 y el 5.
        //->all(); Quitamos el all() para que sea un activeQuery y poder utilizarlo en el dataProvider.

        $consulta2 = Empleado::find()
                ->where(['and', ['codigo' => 1], ['codigo' => 5]]); //Utilizando el parametro or buscamos los codigos que tengan el 1 o el 5.

        $consulta3 = Empleado::find()
                ->where(['or', ['codigo' => 1], ['codigo' => 5]]); //Utilizando el parametro or buscamos los codigos que tengan el 1 o el 5.
        //Listar los empleados cuyo departamento es 1.
        $consulta4 = Empleado::find()
                ->where(['codigo_departamento' => 1]); //Guardamos en una variable la consulta de los empleados que tengan el departamento 1.

        $dataProvider = new ActiveDataProvider([
            'query' => $consulta4 //activeQuery.
        ]);

        return $this->render('empleados', [
                    "registros" => $dataProvider
        ]);
    }

}
