<?php

namespace app\controllers;

use app\models\Departamento;
use app\models\Empleado;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Empleado models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Empleado::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'codigo' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empleado model.
     * @param int $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo) {
        return $this->render('view', [
                    'model' => $this->findModel($codigo),
        ]);
    }

    /**
     * Creates a new Empleado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate() {
        $model = new Empleado();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo' => $model->codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Empleado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo Codigo
     * @return string|Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo) {
        $model = $this->findModel($codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo' => $model->codigo]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Empleado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo Codigo
     * @return Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo) {
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empleado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo Codigo
     * @return Empleado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo) {
        if (($model = Empleado::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsulta1() {
        //Creo el activeQuery.
        $consulta = Empleado::find()
                ->where(['codigo_departamento' => 1]); //Guardamos en una variable la consulta de los empleados que tengan el departamento 1.
        //Crear el dataProvider.
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta //activeQuery.
        ]);

        //Enviar el dataProvider a la vista para usarlo con el widget e imprimirlo.
        return $this->render('listar', [
                    "registros" => $dataProvider
        ]);
    }

    public function actionConsulta2() {
        //Creo el activeQuery.
        $consulta = Empleado::find()
                ->where(['or', ['codigo_departamento' => 1], ['codigo_departamento' => 2]]); //Guardamos en una variable la consulta de los empleados que tengan el departamento 1 o el 2.
        //Crear el dataProvider.
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta //activeQuery.
        ]);

        //Enviar el dataProvider a la vista para usarlo con el widget e imprimirlo.
        return $this->render('listar', [
                    "registros" => $dataProvider
        ]);
    }

    public function actionConsulta3() {
        //Creo el activeQuery.
        $c = Departamento::find()
                        ->where(['nombre' => 'Desarrollo'])
                        ->one()->codigo;

        $consulta = Empleado::find()
                ->where(['codigo_departamento' => $c]);
        //Crear el dataProvider.
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta //activeQuery.
        ]);

        //Enviar el dataProvider a la vista para usarlo con el widget e imprimirlo.
        return $this->render('listar', [
                    "registros" => $dataProvider
        ]);
    }

}
