<?php

use yii\web\View;

/** @var View $this */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas</h1>

        <p class="lead">realizando consultas de seleccion.</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 1</h2>
                <p>Listar los empleados cuyo departamento es 1.</p>
                <?= yii\bootstrap4\Html::a('Ejecutar', ['empleado/consulta1']) ?>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 2</h2>

                <p>Listar los empleados cuyo departamento es el 1 o el 2.</p>
                <?= yii\bootstrap4\Html::a('Ejecutar', ['empleado/consulta2']) ?>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 3</h2>

                <p>Listar los empleados cuyo departamento es desarrollo.</p>
                <?= yii\bootstrap4\Html::a('Ejecutar', ['empleado/consulta3']) ?>
            </div>
        </div>

    </div>
</div>
